package se.plushogskolan.jetbroker;

import static org.junit.Assert.fail;

import java.util.Set;
import java.util.logging.Logger;

import javax.validation.ConstraintViolation;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.customer.domain.Customer;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());
	
	public static void assertPropertyIsInvalid(String property, Set<? extends ConstraintViolation<?>> violations) {


		boolean errorFound = false;
		for (ConstraintViolation<?> constraintViolation : violations) {
			if (constraintViolation.getPropertyPath().toString()
					.equals(property)) {
				errorFound = true;
				break;
			}
		}

		if (!errorFound) {
			fail("Expected validation error for '" + property
					+ "', but no such error exists");
		}
		
	}

	public static Customer getValidCustomer(long id, String fName,
			String lName, String email, String company) {
		Customer customer = new Customer();
		customer.setId(id);
		customer.setFirstName(fName);
		customer.setLastName(lName);
		customer.setEmail(email);
		customer.setCompany(company);

		return customer;
	}
	
	public static Customer getValidCustomer(long id) {
		return getValidCustomer(id, "Johan", "Lindgren", "boo@boo.com", "Plushogskolan");
	}
	
	public static FlightRequest getValidFlightRequest(Customer customer, int noOfPassangers,
			String departureCode, String arrivalCode, DateTime date){
		FlightRequest flightRequest = new FlightRequest();
		flightRequest.setCustomer(customer);
		flightRequest.setNoOfPassangers(noOfPassangers);
		flightRequest.setDepartureCode(departureCode);
		flightRequest.setArrivalCode(arrivalCode);
		flightRequest.setDate(date);
		
		return flightRequest;
	}
	
	
	public static FlightRequest getValidFlightRequest(long id){
		Customer customer = new Customer("Johan", "Lindgren", "boo@boo.com", "Plushogskolan");
		return getValidFlightRequest(customer, 3, "GOT", "MMX", new DateTime(2014, 9, 24, 10, 29, 0, 0));
		
	}

	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}
	
}
