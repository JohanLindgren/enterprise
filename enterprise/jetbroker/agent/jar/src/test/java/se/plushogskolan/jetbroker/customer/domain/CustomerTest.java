package se.plushogskolan.jetbroker.customer.domain;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class CustomerTest {

	@Test
	public void testValidation_firstName_empty() {
		Customer customer = TestFixture.getValidCustomer(0);
		customer.setFirstName("");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("firstName", violations);
	}
	
	@Test
	public void testValidation_lastName_empty() {
		Customer customer = TestFixture.getValidCustomer(0);
		customer.setLastName("");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("lastName", violations);
	}
	
	@Test
	public void testValidation_email_empty() {
		Customer customer = TestFixture.getValidCustomer(0);
		customer.setEmail("Wrong format");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("email", violations);
	}
	
	@Test
	public void testEmptyConstructor() {
		Customer customer = new Customer();
		
		assertNull(customer.getFirstName());
		assertNull(customer.getLastName());
		assertNull(customer.getEmail());
		assertNull(customer.getCompany());
	}

	@Test
	public void testCunstructorForNames() {
		Customer customer = new Customer("Johan", "Lindgren");
		assertEquals("Johan", customer.getFirstName());
		assertEquals("Lindgren", customer.getLastName());
		assertNull(customer.getEmail());
		assertNull(customer.getCompany());

	}

	@Test
	public void testFullConstructor() {
		Customer customer = TestFixture.getValidCustomer(1, "Johan", "Lindgren",
				"boo@boo.com", "Plushogskolan");
		assertEquals("Johan", customer.getFirstName());
		assertEquals("Lindgren", customer.getLastName());
		assertEquals("boo@boo.com", customer.getEmail());
		assertEquals("Plushogskolan", customer.getCompany());
	}
	
	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
}
