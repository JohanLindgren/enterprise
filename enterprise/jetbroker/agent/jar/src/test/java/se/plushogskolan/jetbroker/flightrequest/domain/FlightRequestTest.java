package se.plushogskolan.jetbroker.flightrequest.domain;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class FlightRequestTest {
	
	@Test
	public void testValidation_customer_empty() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		flightRequest.setCustomer(null);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		TestFixture.assertPropertyIsInvalid("customer", violations);
	}
	
	@Test
	public void testValidation_noOfPassangers_rangeOver() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		flightRequest.setNoOfPassangers(501);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		TestFixture.assertPropertyIsInvalid("noOfPassangers", violations);
	}
	
	@Test
	public void testValidation_noOfPassangers_rangeUnder() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		flightRequest.setNoOfPassangers(0);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		TestFixture.assertPropertyIsInvalid("noOfPassangers", violations);
	}
	
	@Test
	public void testStatusCreated() {
		FlightRequest flightRequest = new FlightRequest();
		
		assertEquals(Status.CREATED, flightRequest.getStatus());
	}
	
	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
}
