package se.plushogskolan.jetbroker.flightrequest.repository.jpa;

import javax.inject.Inject;

import static org.junit.Assert.*;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.customer.domain.Customer;
import se.plushogskolan.jetbroker.customer.repository.CustomerRepository;
import se.plushogskolan.jetbroker.customer.repository.jpa.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;
import se.plushogskolan.jetbroker.flightrequest.domain.Status;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends
		AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository> {

	@Inject
	JpaFlightRequestRepository repo;
	
	@Inject
	CustomerRepository customerRepo;
	
	@Test
	public void testSaveAndReadFlightRequest(){
		
		FlightRequest flightRequest = getEntity1();
		Customer customer = flightRequest.getCustomer();
		repo.persist(flightRequest);
		
		FlightRequest flightRequest2 = repo.findById(1);
		Customer customerFromDb = flightRequest2.getCustomer();
		
		assertEquals(Status.CREATED, flightRequest2.getStatus());
		assertEquals(new DateTime(2014, 9, 24, 10, 29, 0, 0), flightRequest2.getDate());
		assertEquals(customer.getEmail(), customerFromDb.getEmail());
		
	}
	
	@Test(expected = Exception.class)
	public void testValidation() {
		
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(null, 200, "GOT", "", null);
		repo.persist(flightRequest);
	}

	@Override
	protected JpaFlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		customerRepo.persist(flightRequest.getCustomer());
		return flightRequest;
	}

	@Override
	protected FlightRequest getEntity2() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		customerRepo.persist(flightRequest.getCustomer());
		return flightRequest;
	}

}
