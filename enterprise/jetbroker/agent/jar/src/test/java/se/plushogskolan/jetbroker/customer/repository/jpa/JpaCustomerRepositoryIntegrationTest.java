package se.plushogskolan.jetbroker.customer.repository.jpa;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.customer.domain.Customer;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaCustomerRepositoryIntegrationTest extends
		AbstractRepositoryTest<Customer, JpaCustomerRepository> {
	
	@Inject
	JpaCustomerRepository repo;

	@Test
	public void testGetAllCustomers() {
		repo.persist(TestFixture.getValidCustomer(0));
		repo.persist(TestFixture.getValidCustomer(0));
		assertEquals(2, repo.getAllCustomers().size());
	}

	@Test(expected = Exception.class)
	public void testValidation() {
		
		Customer customer = TestFixture.getValidCustomer(0,"Johan", "", "boo@boo.com","Plushogskolan");
		repo.persist(customer);
	}
	
	@Override
	protected JpaCustomerRepository getRepository() {
		return repo;
	}

	@Override
	protected Customer getEntity1() {
		return TestFixture.getValidCustomer(0);
	}

	@Override
	protected Customer getEntity2() {
		return TestFixture.getValidCustomer(0);
	}

}