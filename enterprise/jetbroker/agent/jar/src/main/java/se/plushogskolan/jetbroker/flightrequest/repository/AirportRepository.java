package se.plushogskolan.jetbroker.flightrequest.repository;

import java.util.List;

import se.plushogskolan.jetbroker.flightrequest.domain.Airport;

public interface AirportRepository {

	List<Airport> getAllAirports();

}
