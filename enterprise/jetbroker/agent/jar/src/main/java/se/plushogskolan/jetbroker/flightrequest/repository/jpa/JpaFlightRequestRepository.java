package se.plushogskolan.jetbroker.flightrequest.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;
import se.plushogskolan.jetbroker.flightrequest.repository.FlightRequestRepository;

public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return em.createQuery("select f from FlightRequest f").getResultList();
	}

}
