package se.plushogskolan.jetbroker.flightrequest.repository.mockrepository;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.flightrequest.domain.Airport;
import se.plushogskolan.jetbroker.flightrequest.repository.AirportRepository;


public class MockAirportRepository  implements AirportRepository{
	
	List<Airport> airports = new ArrayList<Airport>();
	
	
	private MockAirportRepository(){
		airports.add(new Airport("GOT","Landvetter"));
		airports.add(new Airport("ARN","Arlanda"));
		airports.add(new Airport("MMX","Malmö"));
		airports.add(new Airport("UME","Umeå"));
	}

	@Override
	public List<Airport> getAllAirports() {
		return airports;
		
	}

}
