package se.plushogskolan.jetbroker.flightrequest.domain;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.AirportCode;
import se.plushogskolan.jetbroker.customer.domain.Customer;

/**
 * FlightRequest object
 * 
 * @author Johan
 */
@Entity
public class FlightRequest implements IdHolder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	@ManyToOne 
	private Customer customer;
	@Range(min=1, max=500)
	private int noOfPassangers;
	@NotBlank
	@AirportCode
	private String departureCode;
	@NotBlank
	@AirportCode
	private String arrivalCode;
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime date;
	@NotNull
	@Enumerated
	private Status status = Status.CREATED;
	
	/**
	 * FlightRequest with empty constructor.
	 */
	public FlightRequest() {

	}

	/**
	 * @param customer
	 * @param noOfPassangers
	 * @param departureCode
	 * @param arrivalCode
	 * @param date
	 * 
	 */
	public FlightRequest(Customer customer, int noOfPassangers,
			String departureCode, String arrivalCode, DateTime date) {
		this.customer = customer;
		this.noOfPassangers = noOfPassangers;
		this.departureCode = departureCode;
		this.arrivalCode = arrivalCode;
		this.date = date;
	}
	
	/**
	 * Formates DateTime to pattern: year-month-day hour:minute (dd-MM-yyyy HH:mm)
	 * Format prints month in letters
	 */
	public String getFormatedDate(){
		DateTimeFormatter formatedDate = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
		
		return getDate().toString(formatedDate);
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getNoOfPassangers() {
		return noOfPassangers;
	}

	public void setNoOfPassangers(int noOfPassangers) {
		this.noOfPassangers = noOfPassangers;
	}

	public String getDepartureCode() {
		return departureCode;
	}

	public void setDepartureCode(String departureCode) {
		this.departureCode = departureCode;
	}

	public String getArrivalCode() {
		return arrivalCode;
	}

	public void setArrivalCode(String arrivalCode) {
		this.arrivalCode = arrivalCode;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
