package se.plushogskolan.jetbroker.customer.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.customer.domain.Customer;

@Local
public interface CustomerService {

	Customer getCustomer(long id);

	void updateCustomer(Customer customer);

	Customer createCustomer(Customer customer);

	List<Customer> getAllCustomers();

	void removeCustomer(Customer customer);

}
