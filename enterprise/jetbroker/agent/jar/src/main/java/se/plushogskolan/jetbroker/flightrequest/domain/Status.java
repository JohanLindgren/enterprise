package se.plushogskolan.jetbroker.flightrequest.domain;

/**
 * Status enum
 * 
 * @author Johan
 */
public enum Status {
	CREATED("Created"), REQUEST_COMFIRMED("Request comfirmed"), OFFER_RECEIVED("Offer received"), REJECTED("Rejected");
	
	private String displayName;
	
	
	Status(String displayName){
		this.setDisplayName(displayName);
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
}
