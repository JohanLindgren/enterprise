package se.plushogskolan.jetbroker.customer.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.customer.domain.Customer;
import se.plushogskolan.jetbroker.customer.repository.CustomerRepository;

@Stateless
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerRepository customerRepository;

	@Override
	public Customer getCustomer(long id) {
		return getCustomerRepository().findById(id);
	}

	@Override
	public void updateCustomer(Customer customer) {
		getCustomerRepository().update(customer);

	}

	@Override
	public Customer createCustomer(Customer customer) {
		long id = getCustomerRepository().persist(customer);
		return getCustomer(id);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return getCustomerRepository().getAllCustomers();
	}

	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}

	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public void removeCustomer(Customer customer) {
		getCustomerRepository().remove(customer);
	}

}
