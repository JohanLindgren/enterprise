package se.plushogskolan.jetbroker.customer.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;

/**
 * Customer object
 * 
 * @author Johan
 */
@Entity
public class Customer implements IdHolder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@Email
	private String email;
	private String company;
	@OneToMany(mappedBy="customer")
	private List<FlightRequest> flightRequests;
	

	/**
	 * Customer with empty constructor.
	 */
	public Customer() {
		this(null, null, null, null);
	}

	/**
	 * 
	 * @param fName
	 * @param lName
	 */
	public Customer(String fName, String lName) {
		this(fName, lName, null, null);
	}

	/**
	 * 
	 * @param fName
	 * @param lName
	 * @param email
	 * @param company
	 */
	public Customer(String fName, String lName, String email, String company) {
		setFirstName(fName);
		setLastName(lName);
		setEmail(email);
		setCompany(company);
	}
	
	public List<FlightRequest> getFlightRequests() {
		return flightRequests;
	}
	
	public void setFlightRequests(List<FlightRequest> flightRequests) {
		this.flightRequests = flightRequests;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public long getId() {
		return id;
	}

	public String getFullName() {
		String fullName = getFirstName() + " " + getLastName();
		return fullName;
	}

	/**
	 * Compare object by email
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	/**
	 * Compare object by email
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
}
