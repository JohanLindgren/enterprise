package se.plushogskolan.jetbroker.flightrequest.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;

@Local
public interface FlightRequestService {

	FlightRequest getFlightRequest(long id);

	void updateFlightRequest(FlightRequest flightRequest);

	FlightRequest createFlightRequest(FlightRequest flightRequest);

	List<FlightRequest> getAllFlightRequests();

	void removeFlightRequest(FlightRequest flightRequest);
}
