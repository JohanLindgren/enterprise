package se.plushogskolan.jetbroker.flightrequest.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;
import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.AirportCode;

/**
 * Airport object
 * 
 * @author Johan
 */
public class Airport implements IdHolder, Comparable<Airport> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	@AirportCode
	private String code;
	@NotBlank
	private String name;

	
	/**
	 * Airport with empty constructor.
	 */
	public Airport() {

	}
	
	/**
	 * @param code
	 * @param name
	 */
	public Airport(String code, String name) {
		setCode(code);
		setName(name);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Airport [id=" + id + ", code=" + code + ", name=" + name + "]";
	}

	@Override
	public int compareTo(Airport o) {
		return getName().compareToIgnoreCase(o.getName());
	}

}
