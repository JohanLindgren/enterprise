package se.plushogskolan.jetbroker.flightrequest.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.flightrequest.domain.Airport;

@Local
public interface AirportService {
	
	List<Airport> getAllAirports();

}
