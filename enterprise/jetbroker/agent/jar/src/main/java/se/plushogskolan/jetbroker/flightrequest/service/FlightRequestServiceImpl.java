package se.plushogskolan.jetbroker.flightrequest.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;
import se.plushogskolan.jetbroker.flightrequest.repository.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private FlightRequestRepository flightRequestRepository;

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		getFlightRequestRepository().update(flightRequest);

	}

	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {
		long id = getFlightRequestRepository().persist(flightRequest);
		return getFlightRequest(id);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getFlightRequestRepository().getAllFlightRequests();
	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}

	public void setFlightRequestRepository(FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}

	@Override
	public void removeFlightRequest(FlightRequest flightRequest) {
		getFlightRequestRepository().remove(flightRequest);
	}
}
