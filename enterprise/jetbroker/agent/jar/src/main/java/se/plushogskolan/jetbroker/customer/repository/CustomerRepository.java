package se.plushogskolan.jetbroker.customer.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.customer.domain.Customer;

public interface CustomerRepository extends BaseRepository<Customer> {
	
	List<Customer> getAllCustomers();

}