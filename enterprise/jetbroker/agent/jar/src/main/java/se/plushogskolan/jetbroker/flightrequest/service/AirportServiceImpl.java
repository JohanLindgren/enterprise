package se.plushogskolan.jetbroker.flightrequest.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.flightrequest.domain.Airport;
import se.plushogskolan.jetbroker.flightrequest.repository.AirportRepository;

@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	private AirportRepository airportRepository;

	@Override
	public List<Airport> getAllAirports() {
		return getAirportRepository().getAllAirports();
	}

	public AirportRepository getAirportRepository() {
		return airportRepository;
	}

	public void setAirportRepository(AirportRepository airportRepository) {
		this.airportRepository = airportRepository;
	}

}
