package se.plushogskolan.jetbroker.flightrequest.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;

public interface FlightRequestRepository extends BaseRepository<FlightRequest>{
	
	List<FlightRequest> getAllFlightRequests();

}
