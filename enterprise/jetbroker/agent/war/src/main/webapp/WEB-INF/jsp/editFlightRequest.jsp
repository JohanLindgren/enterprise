<%@ page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<head>
<title>editFlightRequest</title>

<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
	
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<link rel="stylesheet" href="/resources/demos/style.css">

<script>
	$(document).ready(function() {
		
		$(function() {
			$("#datepicker").datepicker({
				dateFormat : 'dd-mm-yy'
			});
		});
	});
</script>


</head>

<body>

	<jsp:include page="header.jsp" />

	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png">
		<c:choose>
			<c:when test="${editFlightRequestBean.id > 0}">
			<spring:message code="flightRequest.editFlight"/>
		</c:when>
			<c:otherwise>
			<spring:message code="index.createNewFlightRequest"/>
		</c:otherwise>
		</c:choose>
	</h2>

	<form:form commandName="editFlightRequestBean">
		<form:hidden path="id" />

		<table class="formTable">
			<c:if test="${editFlightRequestBean.id > 0}">
				<tr>
					<th><spring:message code="global.id"/></th>
					<td>${editFlightRequestBean.id}</td>
					<td></td>
				</tr>
			</c:if>

			<tr>
				<th><spring:message code="agent.customer"/></th>
				<td><form:select path="customerId">

						<form:option value="-1" label=""></form:option>
						<form:options items="${customers}" itemValue="id"
							itemLabel="fullName" />
					</form:select></td>
				<td><form:errors path="customerId" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="agent.departureAirport"/></th>
				<td><form:select path="departureCode">
						<form:option value=""></form:option>
						<c:forEach items="${airports}" var="airport">
							<form:option path="departureCode" value="${airport.code}">${airport.name}, ${airport.code}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="departureCode" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="agent.arrivalAirport"/></th>
				<td><form:select path="arrivalCode">
						<form:option value=""></form:option>
						<c:forEach items="${airports}" var="airport">
							<form:option path="arrivalCode" value="${airport.code}">${airport.name}, ${airport.code}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="arrivalCode" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="agent.noOfPassangers"/></th>
				<td><form:input path="noOfPassangers" /></td>
				<td><form:errors path="noOfPassangers" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="flightRequest.departureDate"/></th>
				<td><form:input path="date" type="text" id="datepicker" /></td>
				<td><form:errors path="date" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="flightRequest.departureTime"/></th>
				<td><form:select path="hour">
						<form:option value="-1" label="" />
						<c:forEach begin="0" end="23" var="hour">
							<form:option value="${hour }">${hour }</form:option>
						</c:forEach>
					</form:select><spring:message code="flightRequest.hour"/></td>
					<td><form:errors path="hour" cssClass="errors" /></td>
			</tr>
			<tr>
				<td></td>
				<td><form:select path="min">
						<form:option value="-1" label="" />
						<c:forEach begin="0" end="59" var="min">
							<form:option value="${min }">${min }</form:option>
						</c:forEach>
					</form:select><spring:message code="flightRequest.minute"/></td>
					<td><form:errors path="min" cssClass="errors" /></td>
			</tr>

			<tr>
				<td><c:set var="submitText">
						<spring:message code="global.submit"/>
					</c:set> <input type="submit" value="${submitText}" /> <a
					href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a></td>
				<td></td>
			</tr>

		</table>
	</form:form>

</body>
</html>