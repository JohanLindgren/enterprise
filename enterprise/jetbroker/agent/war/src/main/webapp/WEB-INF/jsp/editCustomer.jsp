<%@ page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<title>editCustomer</title>

<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />

</head>

<body>

	<jsp:include page="header.jsp" />

	<h2 class="underline">
	<img src="<%=request.getContextPath()%>/images/customer.png">
	<c:choose>
		<c:when test="${editCustomerBean.id > 0}">
			<spring:message code="customer.editCustomer"/>
		</c:when>
		<c:otherwise>
			<spring:message code="index.createNewCustomer"/>
		</c:otherwise>
	</c:choose>
	</h2>

	<form:form commandName="editCustomerBean">
		<form:hidden path="id"/>
		<table class="formTable">
			<c:if test="${editCustomerBean.id > 0}">
			<tr>
				<th><spring:message code="global.id"/></th>
				<td>${editCustomerBean.id}</td>
				<td></td>
			</tr>
			</c:if>
			
			<tr>
				<th><spring:message code="customer.firstName"/></th>
				<td><form:input path="firstName"/></td>
				<td><form:errors path="firstName" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th><spring:message code="customer.lastName"/></th>
				<td><form:input path="lastName"/></td>
				<td><form:errors path="lastName" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th><spring:message code="customer.email"/></th>
				<td><form:input path="email"/></td>
				<td><form:errors path="email" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th><spring:message code="customer.company"/></th>
				<td><form:input path="company"/></td>
				<td><form:errors path="company" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th></th>
				<td>
					<c:set var="submitText">
						<spring:message code="global.submit"/>
					</c:set>
					<input type="submit" value="${submitText}"/> <a href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a></td>
				<td></td>
			</tr>
			
		</table>
	</form:form>

</body>
</html>