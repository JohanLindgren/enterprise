package se.plushogskolan.jetbroker.agent.mvc;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.validation.AirportCode;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;

public class EditFlightRequestBean {

	private long id;
	@Min(value = 0, message = "{validation.flightRequest.customer.missing}")
	private long customerId;
	@Range(min = 1, max = 500)
	private int noOfPassangers;
	@NotBlank(message = "{validation.flightRequest.airport.missing}")
	@AirportCode
	private String departureCode;
	@NotBlank(message = "{validation.flightRequest.airport.missing}")
	@AirportCode
	private String arrivalCode;
	@NotEmpty(message = "{validation.flightRequest.date.missing}")
	@Pattern(regexp = "\\d{2}-\\d{2}-\\d{4}", message = "{validation.flightRequest.date.wrongFormat}")
	private String date;
	@NotNull
	@Range(min = 0, max = 23)
	private Integer hour;
	@NotNull
	@Range(min = 0, max = 59)
	private Integer min;

	public void copyFlightRequestValuesToBean(FlightRequest flightRequest) {
		setId(flightRequest.getId());
		setCustomerId(flightRequest.getCustomer().getId());
		setNoOfPassangers(flightRequest.getNoOfPassangers());
		setDepartureCode(flightRequest.getDepartureCode());
		setArrivalCode(flightRequest.getArrivalCode());
		setDate(flightRequest.getDate().toString(
				DateTimeFormat.forPattern("dd-MM-yyyy")));
		setHour(flightRequest.getDate().getHourOfDay());
		setMin(flightRequest.getDate().getMinuteOfHour());
	}

	public void copyBeanValuesToFlightRequest(FlightRequest flightRequest) {
		flightRequest.setId(getId());
		flightRequest.setNoOfPassangers(getNoOfPassangers());
		flightRequest.setDepartureCode(getDepartureCode());
		flightRequest.setArrivalCode(getArrivalCode());
		flightRequest.setDate(formatDate());
	}

	public DateTime formatDate() {

		DateTimeFormatter format = DateTimeFormat
				.forPattern("dd-MM-yyyy HH:mm");
		String dateAndTime = getDate() + " " + getHour() + ":" + getMin();
		DateTime formatedDate = format.parseDateTime(dateAndTime);
		return formatedDate;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNoOfPassangers() {
		return noOfPassangers;
	}

	public void setNoOfPassangers(int noOfPassangers) {
		this.noOfPassangers = noOfPassangers;
	}

	public String getDepartureCode() {
		return departureCode;
	}

	public void setDepartureCode(String departureCode) {
		this.departureCode = departureCode;
	}

	public String getArrivalCode() {
		return arrivalCode;
	}

	public void setArrivalCode(String arrivalCode) {
		this.arrivalCode = arrivalCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

}
