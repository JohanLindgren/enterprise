package se.plushogskolan.jetbroker.agent.mvc;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.customer.domain.Customer;
import se.plushogskolan.jetbroker.customer.service.CustomerService;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;
import se.plushogskolan.jetbroker.flightrequest.service.FlightRequestService;

@Controller
public class IndexController {

	@Inject
	private CustomerService customerService;

	@Inject
	private FlightRequestService flightRequestService;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		List<Customer> customers = getCustomerService().getAllCustomers();
		List<FlightRequest> flightRequests = getFlightRequestService()
				.getAllFlightRequests();
		
		ModelAndView mav = new ModelAndView("index");
		
		mav.addObject("customers", customers);
		mav.addObject("flightRequests", flightRequests);

		return mav;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(
			FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

}
