package se.plushogskolan.jetbroker.agent.mvc;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.customer.domain.Customer;
import se.plushogskolan.jetbroker.customer.service.CustomerService;
import se.plushogskolan.jetbroker.flightrequest.domain.Airport;
import se.plushogskolan.jetbroker.flightrequest.domain.FlightRequest;
import se.plushogskolan.jetbroker.flightrequest.service.AirportService;
import se.plushogskolan.jetbroker.flightrequest.service.FlightRequestService;

@Controller
@RequestMapping("/editFlightRequest/{id}.html")
public class EditFlightRequestController {
	
	private static Logger log = Logger.getLogger(EditFlightRequestController.class.getName());

	@Inject
	private FlightRequestService flightRequestService;
	
	@Inject
	private CustomerService customerService;
	
	@Inject 
	private AirportService airportService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {
		
		log.fine("Edit flightRequest, id=" + id);

		EditFlightRequestBean bean = new EditFlightRequestBean();

		if (id > 0) {
			FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);
			bean.copyFlightRequestValuesToBean(flightRequest);
		}

		return getModelAndView(bean);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFlightRequestBean bean, BindingResult errors) {

		if (errors.hasErrors()) {
			return getModelAndView(bean);
		}

		if (bean.getId() > 0) {
			FlightRequest flightRequest = getFlightRequestService().getFlightRequest(bean.getId());
			bean.copyBeanValuesToFlightRequest(flightRequest);
			flightRequest.setCustomer(getCustomerService().getCustomer(bean.getCustomerId()));
			getFlightRequestService().updateFlightRequest(flightRequest);
			
		} else {
			FlightRequest flightRequest = new FlightRequest();
			bean.copyBeanValuesToFlightRequest(flightRequest);
			flightRequest.setCustomer(getCustomerService().getCustomer(bean.getCustomerId()));
			getFlightRequestService().createFlightRequest(flightRequest);
		}

		return new ModelAndView("redirect:/index.html");
	}

	
	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public AirportService getAirportService() {
		return airportService;
	}

	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}
	
	private ModelAndView getModelAndView(EditFlightRequestBean bean){
		List<Customer> customers = getCustomerService().getAllCustomers();
		List<Airport> airports = getAirportService().getAllAirports();
		
		ModelAndView mav = new ModelAndView("editFlightRequest");
		mav.addObject("customers", customers);
		mav.addObject("editFlightRequestBean", bean);
		mav.addObject("airports", airports);
		return mav;
	}
	
}
