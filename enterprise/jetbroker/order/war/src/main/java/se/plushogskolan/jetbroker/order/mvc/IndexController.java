package se.plushogskolan.jetbroker.order.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	

	@RequestMapping("/index.html")
	public ModelAndView index() {

		return new ModelAndView("index");
	}

	
}
