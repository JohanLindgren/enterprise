package se.plushogskolan.jetbroker.plane.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private PlaneTypeRepository planeRepository;

	@Override
	public PlaneType getPlaneType(long id) {
		return getPlaneTypeRepository().getPlaneType(id);
	}

	@Override
	public PlaneType createPlaneType(PlaneType planeType) {
		long id = getPlaneTypeRepository().createPlaneType(planeType);
		return getPlaneType(id);
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		getPlaneTypeRepository().updatePlaneType(planeType);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getPlaneTypeRepository().getAllPlaneTypes();
	}

	public PlaneTypeRepository getPlaneTypeRepository() {
		return planeRepository;
	}

	public void setPlaneRepository(PlaneTypeRepository planeRepository) {
		this.planeRepository = planeRepository;
	}

}
