package se.plushogskolan.jetbroker.plane.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.AirportCode;

@Entity
public class Airport implements IdHolder, Comparable<Airport> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	@AirportCode
	private String code;
	@NotBlank
	private String name;
	private double latitude;
	private double longitude;
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime lastUpdated = new DateTime();

	public Airport() {

	}

	public Airport(long id, String code, String name, double lat, double lng) {
		setId(id);
		setCode(code);
		setName(name);
		setLatitude(lat);
		setLongitude(lng);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return "Airport [id=" + id + ", code=" + code + ", name=" + name + "]";
	}

	@Override
	public int compareTo(Airport o) {
		return getName().compareToIgnoreCase(o.getName());
	}

}
