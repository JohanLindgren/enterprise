package se.plushogskolan.jee.exercises.localization;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleExercise {

	public String sayHello(Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise", locale);
		return bundle.getString("hello");
	}

	public String sayName(String name, Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise", locale);
		String string = bundle.getString("sayName");
		
		return MessageFormat.format(string, name);
	}

	public String saySomethingEnglish(Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise", locale);
		return bundle.getString("saySomethingEnglish");
	}

	public String introduceYourself(int age, String city, String name,
			Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise", locale);
		return MessageFormat.format(bundle.getString("myNameIs"), name, age, city);
	}

	public String sayApointmentDate(Date date, Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise", locale);
		String pattern = bundle.getString("letsMeetOn");
		MessageFormat messageFormat = new MessageFormat(pattern, locale);
		Object[] params = new Object[1];
		params[0] = date;
		String format = messageFormat.format(params);
		return format;
	}

	public String sayPrice(double price, Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise", locale);
		String pattern = bundle.getString("currency");
		MessageFormat messageFormat = new MessageFormat(pattern, locale);
		Object[] params = new Object[1];
		params[0] = price;
		String format = messageFormat.format(params);
		return format;
	}

	public String sayFractionDigits(double digits) {
		ResourceBundle bundle = ResourceBundle.getBundle("localizationexercise");
		return MessageFormat.format(bundle.getString("fraction"), digits);
	}
}
