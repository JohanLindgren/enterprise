package se.plushogskolan.jetbroker.agent.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.mock.OrderSystemSimulator;
import se.plushogskolan.jetbroker.agent.services.CustomerService;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;
import se.plushogskolan.jetbroker.agent.services.PlaneService;

@Controller
public class IndexController {

	@Inject
	private CustomerService customerService;

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private PlaneService planeService;

	@Inject
	private OrderSystemSimulator orderSystemSimulator;

	private boolean useMock = false;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		List<FlightRequest> flightRequests = getFlightRequestService().getAllFlightRequests();
		List<FlightRequest> createdRequests = new ArrayList<FlightRequest>();
		List<FlightRequest> offeredRequests = new ArrayList<FlightRequest>();
		List<FlightRequest> bookedRequests = new ArrayList<FlightRequest>();
		List<FlightRequest> rejectedRequests = new ArrayList<FlightRequest>();

		for (FlightRequest request : flightRequests) {
			if (request.getRequestStatus().equals(FlightRequestStatus.CREATED)
					|| request.getRequestStatus().equals(FlightRequestStatus.REQUEST_CONFIRMED)) {
				createdRequests.add(request);
			} else if (request.getRequestStatus().equals(FlightRequestStatus.OFFER_RECEIVED)) {
				offeredRequests.add(request);
			} else if (request.getRequestStatus().equals(FlightRequestStatus.REJECTED)) {
				rejectedRequests.add(request);
			}
		}
		
		List<FlightRequestWrapper> offeredRequestWrappers = wrapRequests(offeredRequests);

		List<Customer> customers = getCustomerService().getAllCustomers();

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("customers", customers);
		mav.addObject("createdRequests", createdRequests);
		mav.addObject("offeredRequests", offeredRequestWrappers);
		mav.addObject("bookedRequests", bookedRequests);
		mav.addObject("rejectedRequests", rejectedRequests);
		mav.addObject("useMock", useMock);
		return mav;
	}

	@RequestMapping("/mockConfirmation/{id}.html")
	public ModelAndView mockConfirmation(@PathVariable long id) {

		orderSystemSimulator.fakeConfirmationFromOrderSystem(id);
		return new ModelAndView("redirect:/index.html");
	}

	@RequestMapping("/mockOffer/{id}.html")
	public ModelAndView mockOffer(@PathVariable long id) {

		orderSystemSimulator.fakeOfferFromOrderSystem(id);
		return new ModelAndView("redirect:/index.html");
	}

	@RequestMapping("/mockRejection/{id}.html")
	public ModelAndView mockRejection(@PathVariable long id) {

		orderSystemSimulator.fakeRejectionFromOrderSystem(id);
		return new ModelAndView("redirect:/index.html");

	}

	/**
	 * Wraps FlightRequests in a FlightRequestWrapper
	 */
	private List<FlightRequestWrapper> wrapRequests(List<FlightRequest> requests) {
		List<FlightRequestWrapper> wrappers = new ArrayList<FlightRequestWrapper>();
		for (FlightRequest req : requests) {
			PlaneType planeType = getPlaneService().getPlaneType(req.getOffer().getPlaneTypeCode());
			wrappers.add(new FlightRequestWrapper(req, planeType));
		}
		return wrappers;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	/**
	 * A wrapper around a FlightRequest so we can also attach a PlaneType to the request.
	 * 
	 */
	public static class FlightRequestWrapper {

		private FlightRequest flightRequest;
		private PlaneType planeType;

		public FlightRequestWrapper(FlightRequest req, PlaneType planeType) {
			setFlightRequest(req);
			setPlaneType(planeType);
		}

		public FlightRequest getFlightRequest() {
			return flightRequest;
		}

		public void setFlightRequest(FlightRequest request) {
			this.flightRequest = request;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

	}

}
