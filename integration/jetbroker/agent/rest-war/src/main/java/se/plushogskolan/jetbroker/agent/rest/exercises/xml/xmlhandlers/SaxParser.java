package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.XmlTestController;

public class SaxParser {

	public static FlightRequest readFlightRequestFromXmlUsingSax(String file)
			throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(false);

		SAXParser saxParser = factory.newSAXParser();
		InputStream in = SaxParser.class.getResourceAsStream(file);
		MyHandler handler = new MyHandler();
		saxParser.parse(in, handler);

		return handler.getParsedFlightRequest();

	}

	static class MyHandler extends DefaultHandler {

		private FlightRequest flightRequest = XmlTestController
				.getMockedFlightRequest();
		private String currentTextValue;

		public FlightRequest getParsedFlightRequest() {
			return flightRequest;
		}

		public void startDocument() throws SAXException {
			flightRequest = new FlightRequest();
		}

		public void startElement(String uri, String localName, String qName,
				Attributes atts) throws SAXException {

			int length = atts.getLength();

			for (int i = 0; i < length; i++) {
				String name = atts.getQName(i);
				String value = atts.getValue(i);

				if (name.equals("id")) {

					flightRequest.setId(Long.valueOf(value));
				}
			}
		}

		public void characters(char ch[], int start, int length)
				throws SAXException {
			currentTextValue = new String(ch, start, length);
		}

		public void endElement(String uri, String localName, String qName)
				throws SAXException {

			if (qName.equalsIgnoreCase("arrivalAirportCode")) {
				flightRequest.setArrivalAirportCode(currentTextValue);
			}

			if (qName.equalsIgnoreCase("departureAirportCode")) {
				flightRequest.setDepartureAirportCode(currentTextValue);
			}

			if (qName.equalsIgnoreCase("noOfPassengers")) {
				flightRequest.setNoOfPassengers(Integer
						.parseInt(currentTextValue));
			}

			if (qName.equals("status")) {
				flightRequest.setRequestStatus(FlightRequestStatus
						.valueOf(currentTextValue));
			}
		}
	}
}