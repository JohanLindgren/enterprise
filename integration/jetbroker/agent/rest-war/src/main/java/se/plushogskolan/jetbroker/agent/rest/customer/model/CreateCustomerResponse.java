package se.plushogskolan.jetbroker.agent.rest.customer.model;

public class CreateCustomerResponse {
	long id;

	public CreateCustomerResponse(long id) {
		setId(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
