package se.plushogskolan.jetbroker.agent.rest.customer;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.agent.rest.customer.model.CreateCustomerRequest;
import se.plushogskolan.jetbroker.agent.rest.customer.model.CreateCustomerResponse;
import se.plushogskolan.jetbroker.agent.rest.customer.model.GetCustomerResponse;
import se.plushogskolan.jetbroker.agent.services.CustomerService;

@Controller
public class CustomerController {

	Logger log = Logger.getLogger(CustomerController.class.getName());

	@Inject
	CustomerService customerService;

	@RequestMapping(value = "/createCustomer", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreateCustomerResponse createCustomer(
			@RequestBody CreateCustomerRequest request) throws Exception {
		log.fine("createCustomer: " + request);

		Customer customer = customerService.createCustomer(request
				.buildCustomer());

		return new CreateCustomerResponse(customer.getId());
	}

	@RequestMapping(value = "/getCustomer/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetCustomerResponse getCustomer(@PathVariable long id) {

		return new GetCustomerResponse(customerService.getCustomer(id));
	}

	@RequestMapping(value = "/deleteCustomer/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deleteCustomer(@PathVariable long id) {
		try {
			customerService.deleteCustomer(id);
			return OkOrErrorResponse.getOkResponse();
		} catch (Exception e) {
			e.printStackTrace();
			return OkOrErrorResponse
					.getErrorResponse("Could not delete Customer");
		}
	}

}
