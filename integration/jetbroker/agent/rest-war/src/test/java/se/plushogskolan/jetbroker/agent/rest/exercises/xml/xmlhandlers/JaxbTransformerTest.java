package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import static org.junit.Assert.*;

import org.junit.Test;

import se.plushogskolan.jetbroker.agent.rest.exercises.xml.XmlTestController;

public class JaxbTransformerTest {

	@Test
	public void testTransformToXml() throws Exception {
		 String xml =
		 JaxbTransformer.transformToXml(XmlTestController.getMockedBoardingCard());
		
		 assertTrue("boarding card", xml.contains("<boardingCard>"));
		 assertTrue("arrivalAirport",
		 xml.contains("<arrivalAirport>Gothenburg</arrivalAirport>"));
		 assertTrue("boardingCardNo",
		 xml.contains("<boardingCardNo>546</boardingCardNo>"));
		 assertTrue("departureDate",
		 xml.contains("<departureDate>2014-04-22 14:45</departureDate>"));
		 assertTrue("departureAirport",
		 xml.contains("<departureAirport>Stockholm</departureAirport>"));
	}

}
