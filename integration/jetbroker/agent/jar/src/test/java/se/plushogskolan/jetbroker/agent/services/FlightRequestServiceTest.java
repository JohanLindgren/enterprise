package se.plushogskolan.jetbroker.agent.services;

import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;


public class FlightRequestServiceTest {
	
	FlightRequest flightRequest;
	
	FlightRequestRepository flightRequestRepository;
	
	FlightRequestServiceImp flightRequestService;
	
	FlightRequestConfirmation flightRequestConfirmation;
	
	OrderFacade orderFacade;

	Customer customer;
	
	@Before
	public void init(){
		customer = TestFixture.getValidCustomer();
		flightRequest = TestFixture.getValidFlightRequest(customer);
		orderFacade = EasyMock.createMock(OrderFacade.class);
		flightRequestRepository = EasyMock.createMock(FlightRequestRepository.class);
		flightRequestConfirmation = new FlightRequestConfirmation(1, 1);
		flightRequestService = new FlightRequestServiceImp();
		flightRequestService.setOrderFacade(orderFacade);
		flightRequestService.setRepository(flightRequestRepository);
		
	}
	
	@Test
	public void testCreateFlightRequest() throws Exception{
		EasyMock.expect(flightRequestRepository.persist(flightRequest)).andReturn(1l);
		orderFacade.sendFlightRequest(flightRequest);
		EasyMock.replay(orderFacade, flightRequestRepository);
		
		flightRequestService.createFlightRequest(flightRequest);
		
		assertEquals("Testing status CREATED ",FlightRequestStatus.CREATED, flightRequest.getRequestStatus());
		
		EasyMock.verify(flightRequestRepository, orderFacade);
		
	}
	
	@Test
	public void testHandleFlightRequestConfirmation() throws Exception{
		EasyMock.expect(flightRequestRepository.findById(1l)).andReturn(flightRequest);
		
		flightRequestRepository.update(flightRequest);
		EasyMock.replay(flightRequestRepository);
		
		flightRequestService.handleFlightRequestConfirmation(flightRequestConfirmation);
		
		assertEquals("Testing status updated to CONFIRMED", FlightRequestStatus.REQUEST_CONFIRMED, flightRequest.getRequestStatus());
		EasyMock.verify(flightRequestRepository);
		
	}

}
