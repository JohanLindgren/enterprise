package se.plushogskolan.jetbroker.agent.integration.order;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public interface OrderFacade {

	public void sendFlightRequest(FlightRequest request) throws Exception;

}
