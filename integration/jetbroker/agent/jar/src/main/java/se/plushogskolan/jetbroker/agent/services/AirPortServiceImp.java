package se.plushogskolan.jetbroker.agent.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class AirPortServiceImp implements AirPortService {
	
	@Inject
	Logger log;

	@Inject
	private AirPortRepository airPortRepository;
	
	@Inject
	@Prod
	PlaneFacade planeFacade;

	@Override
	public List<AirPort> getAllAirPorts() {
		return airPortRepository.getAllAirPorts();
	}

	@Override
	public AirPort getAirPort(String code) {
		return airPortRepository.getAirPort(code);
	}

	@Override
	public void handleAirportsChangedEvent() {
		log.fine("Entering AirPortServiceImp / handleAirportsChanged");
		
		airPortRepository.handleAirportsChangedEvent();
		
	}
}
