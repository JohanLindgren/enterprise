package se.plushogskolan.jetbroker.agent.domain;

/**
 * An airport, coming from the plane system. Has no ID since it is not stored
 * persistently in the agent system. All identification of airports id done
 * using the code variable.
 * 
 */
public class AirPort {
	private String name;
	private String code;

	public AirPort() {

	}

	public AirPort(String code, String name) {
		setCode(code);
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
