package se.plushogskolan.jetbroker.agent.integration.order.mock;

import javax.ejb.Local;

/**
 * A simulation of the action that the order system can trigger on the agent system. Used in a mock environment.
 * 
 */
@Local
public interface OrderSystemSimulator {

	void fakeConfirmationFromOrderSystem(long flightRequestId);

	void fakeOfferFromOrderSystem(long flightRequestId);

	void fakeRejectionFromOrderSystem(long flightRequestId);

}
