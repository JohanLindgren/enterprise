package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class CachedAirPortRepository implements AirPortRepository {

	private static final String ALL_AIRPORTS_KEY = "ALL";

	@Inject
	Logger log;

	@Inject
	@Prod
	private PlaneFacade planeFacade;
	
	private CacheManager cacheManager;
	private Cache cache;
	
	public CachedAirPortRepository(){
		cacheManager = CacheManager.getInstance();
		cache = cacheManager.getCache("airportCache");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AirPort> getAllAirPorts() {
		log.fine("Entering CachedAirPortRepository / getAllAirPorts");
		
		if (!cache.isKeyInCache(ALL_AIRPORTS_KEY)) {
			
			List<AirPort> allAirports = planeFacade.getAllAirports();
			
			cache.put(new Element(ALL_AIRPORTS_KEY, allAirports));
		}
		
		log.fine("AirportList: " + (List<AirPort>) cache.get(ALL_AIRPORTS_KEY).getObjectValue());
		return (List<AirPort>) cache.get(ALL_AIRPORTS_KEY).getObjectValue();
	}

	@Override
	public AirPort getAirPort(String code) {
		log.fine("Entering CachedAirPortRepository / getAirPort");
		
		Element element = cache.get(code);
		
		if(element != null){
			return (AirPort) element.getObjectValue();
		}
		
		List<AirPort> airports = getAllAirPorts();
		
		for(AirPort a : airports){
			if(a.getCode() == code){
				cache.put(new Element(code, a));
				return a;
			}
		}

		log.info("No airport found for code " + code);
		return null;

	}

	@Override
	public void handleAirportsChangedEvent() {
		log.fine("Received airports changed event. ");
		
		cache.removeAll();
	}

}
