package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

@Local
public interface FlightRequestRepository extends BaseRepository<FlightRequest> {

	List<FlightRequest> getAllFlightRequests();

	List<FlightRequest> getFlightRequestsForCustomer(long id);

}
