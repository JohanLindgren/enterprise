package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {
	private static final String ALL_PLANETYPES_KEY = "ALL";

	@Inject
	Logger log;

	@Inject
	@Prod
	private PlaneFacade planeFacade;
	
	private CacheManager cacheManager;
	private Cache cache;
	
	public CachedPlaneTypeRepository(){
		cacheManager = CacheManager.getInstance();
		cache = cacheManager.getCache("planeTypeCache");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("Entering CachedPlaneTypeRepository / getAllPlaneTypes");
		
		if(!cache.isKeyInCache(ALL_PLANETYPES_KEY)){
			List<PlaneType> allPlaneTypes = planeFacade.getAllPlaneTypes();
			
			cache.put(new Element(ALL_PLANETYPES_KEY, allPlaneTypes));
		}

		log.fine("AirportList: " + (List<AirPort>) cache.get(ALL_PLANETYPES_KEY).getObjectValue());
		return (List<PlaneType>) cache.get(ALL_PLANETYPES_KEY).getObjectValue();
	}

	@Override
	public PlaneType getPlaneType(String code) {
		log.fine("Entering getPlaneType with code: " + code);
		
		Element element = cache.get(code);

		if(element != null){
			log.fine("PlaneType found in cache");
			return (PlaneType) element.getObjectValue();
		}
		
		List<PlaneType> planeTypes = getAllPlaneTypes();
		
		for (PlaneType p : planeTypes){
			if(p.getCode() == code){
				cache.put(new Element(code, p));
				log.fine("PlaneType retrieved from list, cache was filled with key " + code);
				return p;
			}
		}

		log.info("No plane type found for code " + code);
		return null;
	}

	@Override
	public void handlePlaneTypeChangedEvent() {
		log.fine("Handle plane types changed event");
		
		cache.removeAll();
	}

}
