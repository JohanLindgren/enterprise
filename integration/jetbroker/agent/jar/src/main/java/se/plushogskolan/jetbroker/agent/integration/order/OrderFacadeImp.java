package se.plushogskolan.jetbroker.agent.integration.order;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

@Prod
public class OrderFacadeImp implements OrderFacade{
	
	@Inject
	Logger log;

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	
	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
	private Queue queue;

	@Override
	public void sendFlightRequest(FlightRequest request) throws Exception {
		log.fine("Entering OrderFacadeImp/sendFlightRequest");
		QueueConnection connection = null;
		QueueSession session = null;
		
		try{
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			
			connection.start();
			
			MapMessage message = session.createMapMessage();
			message.setStringProperty("messageType", JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST);
			message.setString("departureAirportCode", request.getDepartureAirportCode());
			message.setString("arrivalAirportCode", request.getArrivalAirportCode());
			message.setLong("agentRequestId", request.getId());
			message.setInt("noOfPassangers", request.getNoOfPassengers());
			message.setLong("departureTime", request.getDepartureTime().getMillis());
			
			log.fine("Message property " + request);
			
			QueueSender sender = session.createSender(queue);
			sender.send(message);
			
		}catch(Exception e){
			new RuntimeException(e);
		}
		finally{
			connection.close();
			
		}
	}

}
