package se.plushogskolan.jetbroker.agent.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;


@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)})
public class AgentFlightRequestMDB implements MessageListener{

	@Inject
	FlightRequestService flightRequestService;
	
	@Inject
	Logger log;

	@Override
	public void onMessage(Message message) {
		try{
			log.fine("Entering AgentFlightRequestMDB/onMessage");
			
			MapMessage msg = (MapMessage) message;
			
			long agentID = msg.getLong("agentID");
			long confirmationID = msg.getLong("confirmationID");
			
			log.fine("agentID = " + agentID + ", confirmationID = " + confirmationID);
			
			flightRequestService.handleFlightRequestConfirmation(new FlightRequestConfirmation(agentID, confirmationID));
			
		}catch(Exception e){
			log.fine("catch exception: " + e);
			new RuntimeException(e);
		}
		
	}
	
	
}
