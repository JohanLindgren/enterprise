package se.plushogskolan.jetbroker.agent.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.services.AirPortService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = " + "'"+ JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED
				+ "'") })
public class AirportMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	AirPortService airPortService;

	@Override
	public void onMessage(Message message) {
		try {
			log.fine("Entering AirportMDB / onMessage");

			airPortService.handleAirportsChangedEvent();

		} catch (Exception e) {
			new RuntimeException(e);
		}

	}

}
