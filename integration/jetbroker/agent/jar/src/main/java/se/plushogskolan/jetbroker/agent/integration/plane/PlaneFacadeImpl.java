package se.plushogskolan.jetbroker.agent.integration.plane;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;

@Stateless
@Prod
public class PlaneFacadeImpl implements PlaneFacade{
	
	@Inject
	Logger log;

	@Override
	public List<AirPort> getAllAirports() {
		log.fine("Entering PlaneFacadeImpl/getAllAirports");
		
		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();
		PlaneWebService planeWebServicePort = planeWebServiceImplService.getPlaneWebServicePort();
		List<WsAirport> allWsAirPorts = planeWebServicePort.getAirports().getItem();
		List<AirPort> allAirPorts = new ArrayList<AirPort>();
		
		for(WsAirport wsAirport : allWsAirPorts){
			AirPort airPort = new AirPort();
			airPort.setCode(wsAirport.getCode());
			airPort.setName(wsAirport.getName());
			allAirPorts.add(airPort);
		}
		return allAirPorts;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("Entering PlaneFacadeImpl/getAllPlaneTypes");
		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();
		PlaneWebService planeWebServicePort = planeWebServiceImplService.getPlaneWebServicePort();
		List<WsPlaneType> allWsPlaneTypes = planeWebServicePort.getPlaneTypes().getItem();
		List<PlaneType> allPlaneTypes = new ArrayList<PlaneType>();
		
		for(WsPlaneType wsPlaneType : allWsPlaneTypes){
			PlaneType planeType = new PlaneType();
			planeType.setCode(wsPlaneType.getCode());
			planeType.setFuelConsumptionPerKm(wsPlaneType.getFuelConsumptionPerKm());
			planeType.setMaxNoOfPassengers(wsPlaneType.getMaxNoOfPassengers());
			planeType.setMaxRangeKm(wsPlaneType.getMaxRangeKm());
			planeType.setMaxSpeedKmH(wsPlaneType.getMaxSpeedKmH());
			planeType.setName(wsPlaneType.getName());
			allPlaneTypes.add(planeType);
		}
		return allPlaneTypes;
	}

}
