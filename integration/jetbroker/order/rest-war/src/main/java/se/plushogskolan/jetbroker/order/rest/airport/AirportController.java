package se.plushogskolan.jetbroker.order.rest.airport;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.rest.airport.model.GetFuelPriceResponse;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class AirportController {

	Logger log = Logger.getLogger(AirportController.class.getName());

	@Inject
	private PlaneService planeService;

	@RequestMapping(value = "/getFuelPrice", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetFuelPriceResponse getPlaneType() {

		double fuelCost = planeService.getFuelCostPerLiter();

		return new GetFuelPriceResponse(fuelCost);
	}

}
