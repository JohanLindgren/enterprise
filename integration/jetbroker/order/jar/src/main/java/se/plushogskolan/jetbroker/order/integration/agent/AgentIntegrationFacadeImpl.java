package se.plushogskolan.jetbroker.order.integration.agent;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;

@Prod
public class AgentIntegrationFacadeImpl implements AgentIntegrationFacade {
	

	@Inject
	Logger log;

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	
	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)
	private Queue queue;

	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation response) {
		log.fine("Entering AgentIntegrationFacade, sendFlightRequestConfirmation");
		QueueConnection connection = null;
		QueueSession session = null;
		
		log.fine("response agentID = " + response.getAgentRequestId());
		log.fine("response confirmationID = " + response.getOrderRequestId());
		
		try{
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			QueueSender sender = session.createSender(queue);
			connection.start();
			
			MapMessage message = session.createMapMessage();
			message.setStringProperty("messageType", JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE);
			message.setLong("agentID", response.getAgentRequestId());
			message.setLong("confirmationID", response.getOrderRequestId());

			log.fine("Response message prop. agentID = " + message.getLong("agentID"));
			log.fine("Response message prop. confirmadionID = " + message.getLong("confirmationID"));
			
			sender.send(message);
			connection.close();
			
		}catch(Exception e){
			new RuntimeException(e);
		}finally{
			JmsHelper.closeConnectionAndSession(connection, session);
		}
		
	}
	
	@Override
	public void sendUpdatedOfferMessage(FlightRequest flightRequest) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {
		// TODO Auto-generated method stub
		
	}
}
