package se.plushogskolan.jetbroker.order.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {
	private static Logger log = Logger.getLogger(FlightRequestServiceImpl.class.getName());

	@Inject
	private FlightRequestRepository flightRequestRepository;

	@Inject
	private PlaneService planeService;

	@Inject
	@Prod
	private AgentIntegrationFacade agentIntegrationFacade;
	
	@Inject
	private AirportRepository airportRepository;
	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public FlightRequestConfirmation handleIncomingFlightRequest(FlightRequest request) {
		log.fine("Entering FlightRequestService/handleIncomingFlightRequest");
		
		String departureAirportCode = request.getDepartureAirportCode();
		String arrivalAirportCode = request.getArrivalAirportCode();
		
		Airport airport1 = airportRepository.getAirport(departureAirportCode);
		Airport airport2 = airportRepository.getAirport(arrivalAirportCode);
		
		int distance = (int) HaversineDistance.getDistance(airport1.getLatitude(), airport1.getLongitude(), airport2.getLatitude(), airport2.getLongitude());
		request.setDistanceKm(distance);
		log.fine("Distance = " + distance);
		
		request.setStatus(FlightRequestStatus.NEW);
		
		long orderID = flightRequestRepository.persist(request);
		
		FlightRequestConfirmation flightRequestConfirmation = new FlightRequestConfirmation(request.getAgentRequestId(), orderID);
		
		log.fine("FlightRequestServiceImpl - flightRequestId = " + request.getId() + ", status = " + request.getStatus());
		log.fine("agentID = " + request.getAgentRequestId());
		log.fine("orderID = " + orderID);
		
		getAgentIntegrationFacade().sendFlightRequestConfirmation(flightRequestConfirmation);
		
		return flightRequestConfirmation;

	}

	@Override
	public void handleUpdatedOffer(long flightRequestId, Offer offer) {

		// Implement
	}

	@Override
	public void rejectFlightRequest(long id) {

		// Implement

	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getFlightRequestRepository().getAllFlightRequests();
	}

	@Override
	public void deleteFlightRequest(long id) {
		FlightRequest flightRequest = getFlightRequest(id);
		getFlightRequestRepository().remove(flightRequest);

	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}

	public void setFlightRequestRepository(FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public AgentIntegrationFacade getAgentIntegrationFacade() {
		return agentIntegrationFacade;
	}

	public void setAgentIntegrationFacade(AgentIntegrationFacade agentIntegrationFacade) {
		this.agentIntegrationFacade = agentIntegrationFacade;
	}

	public AirportRepository getAirportRepository() {
		return airportRepository;
	}
	
	public void setAirportRepository(AirportRepository airportRepository) {
		this.airportRepository = airportRepository;
	}
}
