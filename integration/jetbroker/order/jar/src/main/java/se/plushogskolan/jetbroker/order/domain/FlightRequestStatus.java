package se.plushogskolan.jetbroker.order.domain;

public enum FlightRequestStatus {

	NEW("New"), OFFER_SENT("Offer sent"), REJECTED_BY_US("Rejected"), BOOKED("Booked");

	private String niceName;

	private FlightRequestStatus(String niceName) {

		this.niceName = niceName;
	}

	public String getNiceName() {
		return niceName;
	}

}
