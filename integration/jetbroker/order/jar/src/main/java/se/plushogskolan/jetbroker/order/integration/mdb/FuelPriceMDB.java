package se.plushogskolan.jetbroker.order.integration.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'FuelPriceChanged'") })
public class FuelPriceMDB extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	private PlaneService planeService;

	public FuelPriceMDB() {

	}

	public void onMessage(Message message) {
		log.fine("FuelPriceMDB/onMessage");
		try {
			double fuelPrice = message.getDoubleProperty("fuelPrice");
			log.fine("fuelpriceMDB onMessage - fuelprice recived is: " + fuelPrice);
			getPlaneService().updateFuelCostPerLiter(fuelPrice);

		} catch (Exception e) {
			logErrorAndRollback(e);
		}

	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

}
