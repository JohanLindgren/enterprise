package se.plushogskolan.jetbroker.order.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;

@Entity
public class FlightRequest implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long agentRequestId;
	private String departureAirportCode;
	private String arrivalAirportCode;
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime departureTime;
	private int noOfPassengers;
	private int distanceKm;
	private FlightRequestStatus status = FlightRequestStatus.NEW;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "offerId")
	private Offer offer;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public DateTime getDepartureTime() {
		return departureTime;
	}

	public Date getDepartureTimeAsDate() {
		return new Date(departureTime.getMillis());
	}

	public void setDepartureTime(DateTime departureTime) {
		this.departureTime = departureTime;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public int getDistanceKm() {
		return distanceKm;
	}

	public void setDistanceKm(int distanceKm) {
		this.distanceKm = distanceKm;
	}

	public FlightRequestStatus getStatus() {
		return status;
	}

	public void setStatus(FlightRequestStatus status) {
		this.status = status;
	}

	public long getAgentRequestId() {
		return agentRequestId;
	}

	public void setAgentRequestId(long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", agentRequestId=" + agentRequestId + ", departureAirportCode="
				+ departureAirportCode + ", arrivalAirportCode=" + arrivalAirportCode + ", departureTime="
				+ departureTime + ", noOfPassengers=" + noOfPassengers + ", distanceKm=" + distanceKm + ", status="
				+ status + ", offer=" + offer + "]";
	}

}
