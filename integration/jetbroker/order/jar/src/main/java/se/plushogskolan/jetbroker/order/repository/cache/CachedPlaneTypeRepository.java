package se.plushogskolan.jetbroker.order.repository.cache;

import java.util.List;

import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

public class CachedPlaneTypeRepository implements PlaneTypeRepository {

	@Inject
	@Prod
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public PlaneType getPlaneType(String code) {

		for (PlaneType planeType : getAllPlaneTypes()) {

			if (planeType.getCode().equals(code)) {
				return planeType;
			}
		}

		return null;

	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return planeIntegrationFacade.getAllPlaneTypes();
	}

}
