package se.plushogskolan.jetbroker.order.integration.plane;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsPlaneType;

@Stateless
@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

	@Inject
	Logger log;

	@Override
	public List<Airport> getAllAirports() {
		log.fine("Entering PlaneIntegrationFacadeImpl/getAllAirports");
		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();
		PlaneWebService planeWebServicePort = planeWebServiceImplService
				.getPlaneWebServicePort();
		List<WsAirport> allWsAirPorts = planeWebServicePort.getAirports()
				.getItem();
		List<Airport> allAirPorts = new ArrayList<Airport>();

		for (WsAirport wsAirport : allWsAirPorts) {
			allAirPorts.add(new Airport(wsAirport.getCode(), wsAirport
					.getName(), wsAirport.getLatitude(), wsAirport
					.getLongitude()));
		}
		return allAirPorts;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("Entering PlaneIntegrationFacadeImpl/getAllplaneTypes");
		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();
		PlaneWebService planeWebServicePort = planeWebServiceImplService
				.getPlaneWebServicePort();
		List<WsPlaneType> allWsPlaneTypes = planeWebServicePort.getPlaneTypes()
				.getItem();
		List<PlaneType> allPlaneTypes = new ArrayList<PlaneType>();

		for (WsPlaneType wsPlaneType : allWsPlaneTypes) {
			PlaneType planeType = new PlaneType();
			planeType.setCode(wsPlaneType.getCode());
			planeType.setFuelConsumptionPerKm(wsPlaneType
					.getFuelConsumptionPerKm());
			planeType.setMaxNoOfPassengers(wsPlaneType.getMaxNoOfPassengers());
			planeType.setMaxRangeKm(wsPlaneType.getMaxRangeKm());
			planeType.setMaxSpeedKmH(wsPlaneType.getMaxSpeedKmH());
			planeType.setName(wsPlaneType.getName());
			allPlaneTypes.add(planeType);
		}
		return allPlaneTypes;
	}

}
