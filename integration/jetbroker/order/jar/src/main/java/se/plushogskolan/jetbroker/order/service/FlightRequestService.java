package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.Offer;

@Local
public interface FlightRequestService {

	FlightRequest getFlightRequest(long id);

	List<FlightRequest> getAllFlightRequests();

	public FlightRequestConfirmation handleIncomingFlightRequest(FlightRequest flightRequest);

	void handleUpdatedOffer(long flightRequestId, Offer offer);

	void rejectFlightRequest(long id);

	void deleteFlightRequest(long id);

}
