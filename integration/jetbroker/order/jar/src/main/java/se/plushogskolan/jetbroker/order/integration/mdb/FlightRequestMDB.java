package se.plushogskolan.jetbroker.order.integration.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;


@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)})
public class FlightRequestMDB extends AbstractMDB implements MessageListener{
	
	@Inject
	Logger log;
	
	@Inject
	FlightRequestService flightRequestService;
	
	public void onMessage(Message message){
		log.fine("Entering FlightRequestMDB in order");
		FlightRequest flightRequest = new FlightRequest();
		try{
			log.fine("FlightRequestMDB/onMessage");
			MapMessage mapMessage = (MapMessage) message;
			
			flightRequest.setAgentRequestId(mapMessage.getLong("agentRequestId"));
			flightRequest.setDepartureAirportCode(mapMessage.getString("departureAirportCode"));
			flightRequest.setArrivalAirportCode(mapMessage.getString("arrivalAirportCode"));
			flightRequest.setNoOfPassengers(mapMessage.getInt("noOfPassangers"));
			flightRequest.setDepartureTime(new DateTime(mapMessage.getLong("departureTime")));

			log.fine("FlightRequestMDB/onMessage - setAgentRequestId = " + flightRequest);
			
			flightRequestService.handleIncomingFlightRequest(flightRequest);
			
		}catch(Exception e){
			new RuntimeException(e);
		}
	}
}
