package se.plushogskolan.jetbroker.order.services;

import static org.junit.Assert.assertEquals;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.order.service.FlightRequestServiceImpl;

public class FlightRequestServiceTest {

	FlightRequest flightRequest;

	FlightRequestRepository flightRequestRepository;

	FlightRequestServiceImpl flightRequestService = new FlightRequestServiceImpl();

	AirportRepository airportRepository;

	AgentIntegrationFacade agentIntegrationFacade;
	
	@Before
	public void init() {
		flightRequest = TestFixture.getValidFlightRequest();
		agentIntegrationFacade = EasyMock
				.createMock(AgentIntegrationFacade.class);
		flightRequestRepository = EasyMock.createMock(FlightRequestRepository.class);
		airportRepository = EasyMock.createMock(AirportRepository.class);
		
	}

	@Test
	public void testHandleIncomingFlightRequest(){
		//Mock airports
		Airport departureAirport = new Airport("GBG", "Gothenburg [Mocked]", 57.697, 11.98);
		Airport arrivalAirport = new Airport("STM", "Stockholm [Mocked]", 59.33, 18.06);
		
		//Capture to read
		Capture<FlightRequest> flightRequestCapture = new Capture<FlightRequest>();
		Capture<FlightRequestConfirmation> flightRequestConfirmationCapture = new Capture<FlightRequestConfirmation>();
		
		flightRequest.setStatus(FlightRequestStatus.REJECTED_BY_US); //Set new status for testing purposes
		
		EasyMock.expect(flightRequestRepository.persist(EasyMock.capture(flightRequestCapture))).andReturn(1l);
		EasyMock.expect(airportRepository.getAirport(flightRequest.getDepartureAirportCode())).andReturn(departureAirport);
		EasyMock.expect(airportRepository.getAirport(flightRequest.getArrivalAirportCode())).andReturn(arrivalAirport);
		
		agentIntegrationFacade.sendFlightRequestConfirmation(EasyMock.capture(flightRequestConfirmationCapture));
		
		EasyMock.replay(flightRequestRepository, airportRepository, agentIntegrationFacade);
		
		//Make mocked connections
		flightRequestService.setAgentIntegrationFacade(agentIntegrationFacade);
		flightRequestService.setFlightRequestRepository(flightRequestRepository);
		flightRequestService.setAirportRepository(airportRepository);
		
		flightRequestService.handleIncomingFlightRequest(flightRequest);
		
		assertEquals(FlightRequestStatus.NEW, flightRequestCapture.getValue().getStatus());
		assertEquals("Expecting orderID = 1", 1, flightRequestConfirmationCapture.getValue().getOrderRequestId());
		assertEquals("Expecting agentID = 10", 10, flightRequestConfirmationCapture.getValue().getAgentRequestId());
		
		EasyMock.verify(flightRequestRepository, airportRepository, agentIntegrationFacade);
	}
}
