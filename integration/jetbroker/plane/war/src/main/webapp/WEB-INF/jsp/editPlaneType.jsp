<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<title><spring:message code="editPlaneType.webtitle"/></title>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<jsp:include page="header.jsp" />
	<h2 class="underline"><img src="<%=request.getContextPath()%>/images/plane.png">
		<c:choose>
			<c:when test="${editPlaneTypeBean.planeType.id > 0}">
			<spring:message code="editPlaneType.title.update"/>
		</c:when>
			<c:otherwise>
			<spring:message code="editPlaneType.title.create"/>
		</c:otherwise>
		</c:choose>
	</h2>

	<form:form commandName="editPlaneTypeBean">
		<form:hidden path="planeType.id" />
		<table class="formTable">
			<c:if test="${editPlaneTypeBean.planeType.id > 0}">
				<tr>
					<th><spring:message code="global.id"/></th>
					<td>${editPlaneTypeBean.planeType.id}</td>
					<td></td>
				</tr>
			</c:if>
			<tr>
				<th><spring:message code="flightrequest.code"/></th>
				<td><form:input path="planeType.code" /></td>
				<td><form:errors path="planeType.code" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="global.name"/></th>
				<td><form:input path="planeType.name" /></td>
				<td><form:errors path="planeType.name" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightrequest.noOfPassengers"/></th>
				<td><form:input path="planeType.maxNoOfPassengers" size="7" maxlength="3" /></td>
				<td><form:errors path="planeType.maxNoOfPassengers" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightrequest.range"/></th>
				<td><form:input path="planeType.maxRangeKm" size="7" maxlength="7" /></td>
				<td><form:errors path="planeType.maxRangeKm" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightrequest.maxSpeed"/></th>
				<td><form:input path="planeType.maxSpeedKmH" size="7" maxlength="4" /></td>
				<td><form:errors path="planeType.maxSpeedKmH" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightrequest.fuelConsumption"/></th>
				<td><form:input path="planeType.fuelConsumptionPerKm" size="7" maxlength="3" /></td>
				<td><form:errors path="planeType.fuelConsumptionPerKm" cssClass="errors" /></td>
			</tr>
			<tr>
				<th></th>
				<td>
					<c:set var="submitText">
						<spring:message code="global.submit" />
					</c:set>
				<input type="submit" value="${submitText}" /> <a href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a></td>
				<td></td>
			</tr>
		</table>
	</form:form>
</body>
</html>