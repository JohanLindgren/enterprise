package se.plushogskolan.jetbroker.plane.service;

import java.util.logging.Logger;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class FuelPriceServiceImpl implements FuelPriceService {

	@Inject
	Logger log;
	
	@Inject
	@Prod
	PlaneIntegrationFacade facade;

	private double cachedFuelCost = 5.2;

	@Override
	@Lock(LockType.READ)
	public double getFuelCost() {
		return cachedFuelCost;
	}

	@Override
	@Lock(LockType.WRITE)
	public void updateFuelCost(double fuelCost) {
		cachedFuelCost = fuelCost;
		facade.broadcastNewFuelPrice(fuelCost);
		
	}
}
