package se.plushogskolan.jetbroker.plane.integration.jetbroker;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;

@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade{
	
	@Inject
	Logger log;

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private TopicConnectionFactory connectionFactory;
	
	@Resource(mappedName = JmsConstants.TOPIC_PLANE_BROADCAST)
	private Topic topic;
	
	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {
		log.fine("Entering broadcastNewFuelPrice");
		
		TopicConnection connection = null;
		TopicSession session = null;
		
		try{
			connection = connectionFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			Message message = session.createMessage();
			message.setDoubleProperty("fuelPrice", fuelPrice);
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED);
			TopicPublisher publisher = session.createPublisher(topic);
			publisher.publish(message);
			log.fine("PlaneIntegrationFacadeImpl sending message: " + message.toString());
			
		} catch(Exception e){
			new RuntimeException(e);
		} finally{
			JmsHelper.closeConnectionAndSession(connection, session);
		}
	}

	@Override
	public void broadcastAirportsChanged() {
		log.fine("Entering PlaneIntegrationFacadeImpl / broadcastAirportsChanged");
		TopicConnection connection  = null;
		TopicSession session = null;
		try{
			connection = connectionFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			Message message = session.createMessage();
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED);
			TopicPublisher publisher = session.createPublisher(topic);
			publisher.publish(message);
			log.fine("PlaneIntegrationFacadeImpl / broadcastAirportsChanged sending message: " + message.toString());
		}catch(Exception e){
			new RuntimeException(e);
		}finally{
			JmsHelper.closeConnectionAndSession(connection, session);
		}
		
	}

	@Override
	public void broadcastPlaneTypesChanged() {
		log.fine("Entering PlaneIntegrationFacadeImpl / broadcastPlaneTypesChanged");
		TopicConnection connection  = null;
		TopicSession session = null;
		try{
			connection = connectionFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			Message message = session.createMessage();
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED);
			TopicPublisher publisher = session.createPublisher(topic);
			publisher.publish(message);
			log.fine("PlaneIntegrationFacadeImpl / broadcastPlaneTypesChanged sending message: " + message.toString());
		}catch(Exception e){
			new RuntimeException(e);
		}finally{
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

}
