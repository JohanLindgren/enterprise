package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Stateless
@WebService(name = "PlaneWebService", endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService")
public class PlaneWebServiceImpl implements PlaneWebService {

	@Inject
	Logger log;

	@Inject
	AirportService airportService;

	@Inject
	PlaneService planeService;

	@Override
	public List<WSAirport> getAirports() {
		log.fine("Entering PlaneWebServiceImpl/getAirports");

		List<WSAirport> wsAirportList = new ArrayList<WSAirport>();
		List<Airport> allAirports = airportService.getAllAirports();

		for (Airport airport : allAirports) {
			WSAirport wsAirport = new WSAirport();
			wsAirport.setCode(airport.getCode());
			wsAirport.setLatitude(airport.getLatitude());
			wsAirport.setLongitude(airport.getLongitude());
			wsAirport.setName(airport.getName());
			wsAirportList.add(wsAirport);

		}
		log.fine("Returning list of WSAirports: " + wsAirportList.toString());
		return wsAirportList;
	}

	@Override
	public List<WSPlaneType> getPlaneTypes() {
		log.fine("Entering PlaneWebServiceImpl/getPlaneTypes");

		List<WSPlaneType> wsPlaneTypeList = new ArrayList<WSPlaneType>();
		List<PlaneType> planeTypeList = planeService.getAllPlaneTypes();

		for (PlaneType planeType : planeTypeList) {
			WSPlaneType wsPlaneType = new WSPlaneType();
			wsPlaneType.setCode(planeType.getCode());
			wsPlaneType.setFuelConsumptionPerKm(planeType
					.getFuelConsumptionPerKm());
			wsPlaneType.setMaxNoOfPassengers(planeType.getMaxNoOfPassengers());
			wsPlaneType.setMaxRangeKm(planeType.getMaxRangeKm());
			wsPlaneType.setMaxSpeedKmH(planeType.getMaxSpeedKmH());
			wsPlaneType.setName(planeType.getName());
			wsPlaneTypeList.add(wsPlaneType);
		}
		log.fine("Returning list of WSPlaneTypes: "
				+ wsPlaneTypeList.toString());
		return wsPlaneTypeList;
	}

}
