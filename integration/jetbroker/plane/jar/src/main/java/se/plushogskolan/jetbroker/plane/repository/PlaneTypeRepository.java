package se.plushogskolan.jetbroker.plane.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;

public interface PlaneTypeRepository extends BaseRepository<PlaneType> {

	List<PlaneType> getAllPlaneTypes();

}
