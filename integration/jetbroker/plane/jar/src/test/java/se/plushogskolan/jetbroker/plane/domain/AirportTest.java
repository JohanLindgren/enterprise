package se.plushogskolan.jetbroker.plane.domain;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Test;

import se.plushogskolan.jetbroker.PropertyValidator;
import se.plushogskolan.jetbroker.TestFixture;

public class AirportTest {

	PropertyValidator<Airport> propertyValidator = new PropertyValidator<Airport>();

	@Test
	public void testValidation_code_empty() {
		Airport airport = TestFixture.getValidAirport();
		airport.setCode("");

		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("code", violations);
	}

	@Test
	public void testValidation_code_null() {
		Airport airport = TestFixture.getValidAirport();
		airport.setCode(null);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("code", violations);
	}

	@Test
	public void testValidation_code_ok() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName("GBG");
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		assertTrue("No validation errors for code", violations.isEmpty());
	}

	@Test
	public void testValidation_name_empty() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName("");
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("name", violations);
	}

	@Test
	public void testValidation_name_null() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName(null);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("name", violations);
	}

	@Test
	public void testValidation_name_ok() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName("City Airport");
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		assertTrue("No validation errors for name", violations.isEmpty());
	}

	@Test
	public void testValidation_latitude_zero() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLatitude(0);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("latitude", violations);
	}

	@Test
	public void testValidation_latitude_too_big() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLatitude(200);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("latitude", violations);
	}

	@Test
	public void testValidation_latitude_too_small() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLatitude(-200);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("latitude", violations);
	}

	@Test
	public void testValidation_latitude_ok() {
		Airport airport = TestFixture.getValidAirport();
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		assertTrue("No validation errors for latitude", violations.isEmpty());
	}

	@Test
	public void testValidation_longitude_zero() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLongitude(0);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("longitude", violations);
	}

	@Test
	public void testValidation_longitude_too_big() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLongitude(200);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("longitude", violations);
	}

	@Test
	public void testValidation_longitude_too_small() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLongitude(-200);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		propertyValidator.assertPropertyIsInvalid("longitude", violations);
	}

	@Test
	public void testValidation_longitude_ok() {
		Airport airport = TestFixture.getValidAirport();
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(airport);
		assertTrue("No validation errors for longitude", violations.isEmpty());
	}

	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
}
