package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LatitudeValidator implements ConstraintValidator<Latitude, Object> {

	@Override
	public void initialize(Latitude lat) {

	}

	@Override
	public boolean isValid(Object valueAsObject, ConstraintValidatorContext context) {

		if (valueAsObject == null) {
			return true;
		}

		try {
			double value = 0;
			if (valueAsObject instanceof String) {
				value = Double.parseDouble((String) valueAsObject);
			} else if (valueAsObject instanceof Integer) {
				value = Integer.valueOf((Integer) valueAsObject);
			} else if (valueAsObject instanceof Double) {
				value = (Double) valueAsObject;
			}

			if (value == 0) {
				return false;
			} else if (value < -90 || value > 90) {
				return false;
			} else {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		}

	}

}
