package se.plushogskolan.cdi.model;

public class Car {

	private String name;
	private Owner owner;

	public Car(String name) {
		setName(name);
	}

	public Car(String name, Owner owner) {
		setName(name);
		setOwner(owner);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Car [name=" + name + ", owner=" + owner.getName() + "]";
	}

}
