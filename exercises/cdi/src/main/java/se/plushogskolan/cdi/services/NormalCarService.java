package se.plushogskolan.cdi.services;

import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.Environment.EnvironmentLevel;
import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.Income.IncomeLevel;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.model.Owner;
import se.plushogskolan.cdi.repository.CarRepository;

@Income(IncomeLevel.MEDIUM)
@Environment(EnvironmentLevel.MOCK)
@InvocationCounter
public class NormalCarService implements CarService {

	/**
	 * Injecting a medium income owner. The Owner is created using the @Producer
	 * method in Owner.java.
	 */
	@Inject
	@Income(IncomeLevel.MEDIUM)
	private Owner owner;

	@Inject
	@Environment(EnvironmentLevel.MOCK)
	private CarRepository carRepository;

	public Car getCar() {
		Car car = carRepository.getNormalCar();
		car.setOwner(owner);
		return car;
	}

	public void saveCar(Car car) {
		carRepository.saveCar(car);

	}

	@Override
	public String toString() {
		return "NormalCarService";
	}

}
