package se.plushogskolan.cdi.repository.mock;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.Environment.EnvironmentLevel;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.repository.CarRepository;

/**
 * A mocked repository
 * 
 */
@Environment(EnvironmentLevel.MOCK)
@InvocationCounter
public class MockCarRepository implements CarRepository {

	public MockCarRepository() {
	}

	public Car getNormalCar() {
		return new Car("Mocked normal car");
	}

	public Car getSportsCar() {
		return new Car("Mocked sports car");
	}

	public void saveCar(Car car) {
		System.out.println("Save car in mock: " + car.getName());
	}

}
