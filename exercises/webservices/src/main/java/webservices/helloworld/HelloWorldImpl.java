package webservices.helloworld;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Implement the HelloWorld web service
 */
@WebService(endpointInterface = "webservices.helloworld.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	public static void main(String[] args) {
		Endpoint.publish(
				"http://localhost:8081/helloWorld", 
				new HelloWorldImpl()
				);
	}

	public String helloWorld() {
		return "Hello world";
	}

}
